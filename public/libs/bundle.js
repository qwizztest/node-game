(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/index.js":[function(require,module,exports){
module.exports = require('media').audio;

},{"media":"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/index.js"}],"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/index.js":[function(require,module,exports){
module.exports = require('./lib/player');
module.exports.audio = media('audio');
module.exports.video = media('video');

function media (kind) {
  return function (urls, dom) {
    return module.exports(kind, urls, dom);
  };
}

},{"./lib/player":"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/lib/player.js"}],"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/lib/mime.js":[function(require,module,exports){
var table = {
  aif  : "audio/x-aiff",
  aiff : "audio/x-aiff",
  wav  : "audio/x-wav",
  mp3  : 'audio/mpeg',
  m3u  : "audio/x-mpegurl",
  mid  : "audio/midi",
  midi : "audio/midi",
  m4a  : 'audio/m4a',
  ogg  : 'audio/ogg',
  mp4  : 'video/mp4',
  ogv  : 'video/mp4',
  webm : 'video/webm',
  mkv  : 'video/x-matroska',
  mpg  : 'video/mpeg'
};

module.exports = mimeOf;

function mimeOf(url){
  return table[ url.split('.').slice(-1)[0] ];
}

},{}],"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/lib/player.js":[function(require,module,exports){
var newChain  = require('new-chain'),
    src = require('./src'),
    render = require('./render');

module.exports = play;

function play(media, urls, dom){
  var el, chain, url;

  dom || ( dom = document.documentElement );
  el = render(media);
  dom.appendChild(el);

  chain = newChain({
    autoplay: bool('autoplay'),
    controls: bool('controls'),
    load: method('load'),
    loop: bool('loop'),
    muted: bool('muted'),
    on: on,
    pause: method('pause'),
    play: method('play'),
    preload: bool('preload')
  });

  chain.currentTime = attr('currentTime');
  chain.element = element;
  chain.src = src.attr(el);
  chain.volume = attr('volume');
  chain.remove = remove;

  chain.src(urls);

  return chain;

  function attr(name){
    return function(value){
      if ( arguments.length ) {
        el[name] = value;
        return chain;
      }

      return el[name];
    };
  }

  function bool(name){
    return function(value){
      if (value === false) {
        return el[name] = false;
      }

      return el[name] = true;
    };
  }

  function element(){
    return el;
  }

  function on(event, callback){
    el.addEventListener(event, callback, false);
  }

  function method(name){
    return function(){
      return el[name].apply(el, arguments);
    };
  }

  function remove(){
    return el.parentNode.removeChild(el);
  }

}

},{"./render":"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/lib/render.js","./src":"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/lib/src.js","new-chain":"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/node_modules/new-chain/index.js"}],"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/lib/render.js":[function(require,module,exports){
var domify = require('domify'),
    templates = require("./templates");

module.exports = render;

function render(media){
  return domify(templates[media + '.html']);
}

},{"./templates":"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/lib/templates.js","domify":"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/node_modules/domify/index.js"}],"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/lib/src.js":[function(require,module,exports){
var mimeOf = require("./mime");

module.exports = {
  attr: attr,
  pick: pick
};

function attr(el){
  var value;

  return function(urls){
    if (arguments.length) {
      value = urls;
      el.setAttribute('src', pick(el, value));
    }

    return value;
  };
}

function pick(el, urls){
  if(!urls) return;

  if(typeof urls == 'string'){
    return urls;
  }

  var canPlay = urls.filter(function (url) {
    return !!el.canPlayType(url.mime || mimeOf(url.type || url));
  });

  if (canPlay.length == 0) return;

  return canPlay[0].url || canPlay[0];
}

},{"./mime":"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/lib/mime.js"}],"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/lib/templates.js":[function(require,module,exports){
exports["audio.html"] = "<audio preload=\"auto\" /></audio>"
exports["video.html"] = "<video preload=\"auto\" /></video>"
},{}],"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/node_modules/domify/index.js":[function(require,module,exports){

/**
 * Expose `parse`.
 */

module.exports = parse;

/**
 * Wrap map from jquery.
 */

var map = {
  option: [1, '<select multiple="multiple">', '</select>'],
  optgroup: [1, '<select multiple="multiple">', '</select>'],
  legend: [1, '<fieldset>', '</fieldset>'],
  thead: [1, '<table>', '</table>'],
  tbody: [1, '<table>', '</table>'],
  tfoot: [1, '<table>', '</table>'],
  colgroup: [1, '<table>', '</table>'],
  caption: [1, '<table>', '</table>'],
  tr: [2, '<table><tbody>', '</tbody></table>'],
  td: [3, '<table><tbody><tr>', '</tr></tbody></table>'],
  th: [3, '<table><tbody><tr>', '</tr></tbody></table>'],
  col: [2, '<table><tbody></tbody><colgroup>', '</colgroup></table>'],
  _default: [0, '', '']
};

/**
 * Parse `html` and return the children.
 *
 * @param {String} html
 * @return {Array}
 * @api private
 */

function parse(html) {
  if ('string' != typeof html) throw new TypeError('String expected');

  // tag name
  var m = /<([\w:]+)/.exec(html);
  if (!m) throw new Error('No elements were generated.');
  var tag = m[1];

  // body support
  if (tag == 'body') {
    var el = document.createElement('html');
    el.innerHTML = html;
    return el.removeChild(el.lastChild);
  }

  // wrap map
  var wrap = map[tag] || map._default;
  var depth = wrap[0];
  var prefix = wrap[1];
  var suffix = wrap[2];
  var el = document.createElement('div');
  el.innerHTML = prefix + html + suffix;
  while (depth--) el = el.lastChild;

  var els = el.children;
  if (1 == els.length) {
    return el.removeChild(els[0]);
  }

  var fragment = document.createDocumentFragment();
  while (els.length) {
    fragment.appendChild(el.removeChild(els[0]));
  }

  return fragment;
}

},{}],"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/node_modules/media/node_modules/new-chain/index.js":[function(require,module,exports){
module.exports = newChain;
module.exports.from = from;

function from(chain){

  return function(){
    var m, i;

    m = methods.apply(undefined, arguments);
    i   = m.length;

    while ( i -- ) {
      chain[ m[i].name ] = m[i].fn;
    }

    m.forEach(function(method){
      chain[ method.name ] = function(){
        method.fn.apply(this, arguments);
        return chain;
      };
    });

    return chain;
  };

}

function methods(){
  var all, el, i, len, result, key;

  all    = Array.prototype.slice.call(arguments);
  result = [];
  i      = all.length;

  while ( i -- ) {
    el = all[i];

    if ( typeof el == 'function' ) {
      result.push({ name: el.name, fn: el });
      continue;
    }

    if ( typeof el != 'object' ) continue;

    for ( key in el ) {
      result.push({ name: key, fn: el[key] });
    }
  }

  return result;
}

function newChain(){
  return from({}).apply(undefined, arguments);
}

},{}],"/Users/qwizz/projects/node/chat-example/public/game.js":[function(require,module,exports){
// Solves chrome for andriod issue 178297 Require user gesture
// https://code.google.com/p/chromium/issues/detail?id=178297
// Fix based on code from http://blog.foolip.org/2014/02/10/media-playback-restrictions-in-blink/
if (mediaPlaybackRequiresUserGesture()) {
    window.addEventListener('keydown', removeBehaviorsRestrictions);
    window.addEventListener('mousedown', removeBehaviorsRestrictions);
    window.addEventListener('touchstart', removeBehaviorsRestrictions);
}

function mediaPlaybackRequiresUserGesture() {
    // test if play() is ignored when not called from an input event handler
    var video = document.createElement('video');
    video.play();
    return video.paused;
}

function removeBehaviorsRestrictions() {
    for (var i = 0; i < audioElements.length; i++) {
        audioElements[i].load();
    }

    window.removeEventListener('keydown', removeBehaviorsRestrictions);
    window.removeEventListener('mousedown', removeBehaviorsRestrictions);
    window.removeEventListener('touchstart', removeBehaviorsRestrictions);
}

var playAudio = require('play-audio');

    jQuery(function($){   
        
        'use strict';

        
        var shot_a = playAudio(['snd/shot.ogg','snd/shot.mp3']);
        var win_a = playAudio(['snd/win.ogg','snd/win.mp3']);
        var start_a = playAudio(['snd/start_snd.ogg','snd/start_snd.mp3']);
        var bg_a = playAudio(['snd/bg_loop.ogg','snd/bg_loop.mp3']);
        var intro_a = playAudio(['snd/intro.ogg','snd/intro.mp3']);
        intro_a.preload();
        win_a.preload();
        start_a.preload();
        shot_a.preload();
        bg_a.preload();

        var socket = io();
        $('#output').css('width: 100%');
        $('#output').css('height: 70%;');
        textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});

        function shot_flash() {
        $('#canvas').fadeTo(10,0).fadeTo(300,1);
        $('#flasher').show().fadeTo(550,0);
        shot_a.play();
        };
      
    //outputs    
        
        $('form').submit(function(){
            socket.emit('chat message', $('#m').val());
            $('#messages').append($('<li>').text('ME: '+$('#m').val()));
            $('#m').val('');
            return false;
        });
        
        $('#fireBtn').click(function(){
            console.log('fire triggered.'+socket.id);
            socket.emit('shot', socket.id);
        });
        
        $('#resetBtn').click(function(){
            location.reload(true);
        });
        
    
    //inputs
        
      socket.on('intro msg', function(msg){

        //audio.playSound('bg');

        if (confirm(msg+' You ready for this?')) {
          socket.emit('init msg',true);
          bg_a.play().loop();
          $('#output').show();
          textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
        }
        else{
          socket.emit('init msg',false);
        }
      });

    socket.on('upd', function(msg){
        console.log('update received.');
        switch (msg[0]) {
                case 'init':
                    bg_a.pause();
                    intro_a.play();
                    $('#output').show();
                    $('#output').css('width: 100%');
                    $('#output').css('height: 70%;');
                    textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
                    $('#output').html("Ready?");
                    textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
                    $('#shooter2').show();
                    break;

                case 'log':
                    console.log('data: '+msg[1]);
                    start_a.play();                   
                    $('#fireBtn').show();
                    $('#output').html("DRAW!");
                    break;

                case 'timer':
                    var truncTime = msg[1];
                    $('#output').html(truncTime.toString().substr(0,3));
                    textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
                    break;

                case 'full':
                    alert('game full. try later.');
                    socket.disconnect();
                    break;

                case 'winner':
                    shot_flash();

                    $('#shooter1').hide();
                    $('#shooting1').show();
                    $('#shooter2').hide();
                    $('#shot2').show();

                    $('#output').innerHTML = "BOOM HEADSHOT!  You shot 1st.";
                    textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
                    console.log('BOOM HEADSHOT!  You shot 1st.');
                    win_a.play();
                    break;

                case 'loser':
                    shot_flash();

                    $('#shooter2').hide();
                    $('#shooting2').show();
                    $('#shooter1').hide();
                    $('#shot1').show();

                    $('#output').innerHTML = 'How\'s your face? SHOT. That\'s how your face is.  BANGBANG.';
                    textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
                    break;

                case 'gameover':
                    console.log('game over.');
                    socket.disconnect();
                    $('#fireBtn').hide();
                    $('#resetBtn').show();
                    

                    break;
        }
      });


      socket.on('chat message', function(msg){
        $('#messages').append($('<li>').text(msg));
      });
        
    });
},{"play-audio":"/Users/qwizz/projects/node/chat-example/node_modules/play-audio/index.js"}]},{},["/Users/qwizz/projects/node/chat-example/public/game.js"]);
