// Solves chrome for andriod issue 178297 Require user gesture
// https://code.google.com/p/chromium/issues/detail?id=178297
// Fix based on code from http://blog.foolip.org/2014/02/10/media-playback-restrictions-in-blink/
if (mediaPlaybackRequiresUserGesture()) {
    window.addEventListener('keydown', removeBehaviorsRestrictions);
    window.addEventListener('mousedown', removeBehaviorsRestrictions);
    window.addEventListener('touchstart', removeBehaviorsRestrictions);
}

function mediaPlaybackRequiresUserGesture() {
    // test if play() is ignored when not called from an input event handler
    var video = document.createElement('video');
    video.play();
    return video.paused;
}

function removeBehaviorsRestrictions() {
    for (var i = 0; i < audioElements.length; i++) {
        audioElements[i].load();
    }

    window.removeEventListener('keydown', removeBehaviorsRestrictions);
    window.removeEventListener('mousedown', removeBehaviorsRestrictions);
    window.removeEventListener('touchstart', removeBehaviorsRestrictions);
}

var playAudio = require('play-audio');

    jQuery(function($){   
        
        'use strict';

        
        var shot_a = playAudio(['snd/shot.ogg','snd/shot.mp3']);
        var win_a = playAudio(['snd/win.ogg','snd/win.mp3']);
        var start_a = playAudio(['snd/start_snd.ogg','snd/start_snd.mp3']);
        var bg_a = playAudio(['snd/bg_loop.ogg','snd/bg_loop.mp3']);
        var intro_a = playAudio(['snd/intro.ogg','snd/intro.mp3']);
        intro_a.preload();
        win_a.preload();
        start_a.preload();
        shot_a.preload();
        bg_a.preload();

        var socket = io();
        $('#output').css('width: 100%');
        $('#output').css('height: 70%;');
        textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});

        function shot_flash() {
        $('#canvas').fadeTo(10,0).fadeTo(300,1);
        $('#flasher').show().fadeTo(550,0);
        shot_a.play();
        };
      
    //outputs    
        
        $('form').submit(function(){
            socket.emit('chat message', $('#m').val());
            $('#messages').append($('<li>').text('ME: '+$('#m').val()));
            $('#m').val('');
            return false;
        });
        
        $('#fireBtn').click(function(){
            console.log('fire triggered.'+socket.id);
            socket.emit('shot', socket.id);
        });
        
        $('#resetBtn').click(function(){
            location.reload(true);
        });
        
    
    //inputs
        
     socket.on('connected', function(){
	 console.log("welcome.");
	 bg_a.play().loop();
	 $('#output').show();
                    $('#output').css('width: 100%');
                    $('#output').css('height: 70%;');
                    textFit($('#output'),{minFontSize:20, maxFontSize: 250, alignHoriz: true, alignVert: true});
                    $('#output').html("Start your own room and wait <br> for a challenger or randomly join a session!");
                    textFit($('#output'),{minFontSize:20, maxFontSize: 250, alignHoriz: true, alignVert: true});
	 $('#btnBank1').show();
	 });

//Basic, random shooting
	 $('#randomFight').click(function(){
          socket.emit('init msg',{status: 'on'});
          $('#btnBank1').hide();
		  $('#shooter1').show();
		  $('#output').html("Waiting for another shooter...");
          textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
     });

	 
 //Private Room setup
 	 $('#createRoom').click(function(){
          $('#btnBank1').hide();
		  $('#output').html("Enter a room label:");
          textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
		  $('#rmMaker').show();
     });
	 
//Private Room
	 $('#initRoom').click(function(){
		socket.emit('init msg',{status: 'on', roomLabel: $('#roomLabel').val()});
          $('#btnBank1').hide();
		  $('#shooter1').show();
		  $('#rmMaker').hide();
		  $('#output').html("Waiting for another shooter...");
          textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
	 });
	 
    socket.on('upd', function(msg){
        console.log('update received.');
        switch (msg[0]) {
                case 'init':
                    bg_a.pause();
                    intro_a.play();
                    $('#output').show();
                    $('#output').css('width: 100%');
                    $('#output').css('height: 70%;');
                    textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
                    $('#output').html("Ready?");
                    textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
                    $('#shooter2').show();
                    break;

                case 'log':
                    console.log('data: '+msg[1]);
                    start_a.play();                   
                    $('#fireBtn').show();
                    $('#output').html("DRAW!");
                    break;

                case 'timer':
                    var truncTime = msg[1];
                    $('#output').html(truncTime.toString().substr(0,3));
                    textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
                    break;

                case 'full':
                    alert('game full. try later.');
                    socket.disconnect();
                    break;

                case 'winner':
                    shot_flash();

                    $('#shooter1').hide();
                    $('#shooting1').show();
                    $('#shooter2').hide();
                    $('#shot2').show();

                    $('#output').innerHTML = "BOOM HEADSHOT!  You shot 1st.";
                    textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
                    console.log('BOOM HEADSHOT!  You shot 1st.');
                    win_a.play();
                    break;

                case 'loser':
                    shot_flash();

                    $('#shooter2').hide();
                    $('#shooting2').show();
                    $('#shooter1').hide();
                    $('#shot1').show();

                    $('#output').innerHTML = 'How\'s your face? SHOT. That\'s how your face is.  BANGBANG.';
                    textFit($('#output'),{minFontSize:20, maxFontSize: 100, alignHoriz: true, alignVert: true});
                    break;

                case 'gameover':
                    console.log('game over.');
                    socket.disconnect();
                    $('#fireBtn').hide();
                    $('#resetBtn').show();
                    

                    break;
        }
      });


      socket.on('chat message', function(msg){
        $('#messages').append($('<li>').text(msg));
      });
        
    });