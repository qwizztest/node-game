# TODO #

* Use callbacks or something for the "game over" message after the "winner/loser" decision messages are sent.  Sometimes the "Game Over" alert arrives before the decision message.
* Implement browserify or other bundling procedure.
* Graphics!!
* Express can specify one folder to serve static files from instead of referencing each one; I just haven't figured out how to make it happen.  index.js