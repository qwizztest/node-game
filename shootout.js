var io;
var gameSocket
var shooters = [];
var timer;
var dagame;
var output;
var currentTime;
var mySocket;
	
exports.initGame = function(sio, socket){
    io = sio;
    gameSocket = socket;
    gameSocket.emit('connected', { message: "You are connected!" });
	console.log('a user connected. Are they down for the scrap?');
	
	gameSocket.emit('intro msg', "Welcome to the shootout.");	

    // Host Events
	gameSocket.on('init msg', initMsg);
	gameSocket.on('chat message', chatMessage);

    // Player Events
    gameSocket.on('shot', shot);
	gameSocket.on('disconnect', disconnectG);

}

/* *******************************
   *                             *
   *       HOST FUNCTIONS        *
   *                             *
   ******************************* */



function Timer(){
  //simple timer
    
    this.reset = function(){
        this.date = new Date();
        this.startTime = this.date.getTime();
        this.elapsedTime = 0;    
    } // end reset
    
    
    this.getCurrentTime = function(){
        this.date = new Date();
        return this.date.getTime();
    } // end getCurrentTime
    
    this.getElapsedTime = function(){
        current = this.getCurrentTime();
        return (current - this.startTime) / 1000;
    } // end getElapsedTime
    
    this.reset();
} // end Timer def


	

 function update(){
        currentTime = timer.getElapsedTime();
        io.sockets.emit('upd',['timer',currentTime]);
        //console.log(currentTime);
    } 




function initMsg(msg){
	var room = msg['roomLabel'];
	var shootersLocal=[];
	
	
	//Random Game
		if (msg['status']=='on') {
		var priv;
		if (room) {
			priv = true;
		}
		
			if (shooters.length < 2) {
                console.log('Awwww shit... ' + gameSocket.id + ' is ready to get it in.');
                if (priv) {
					shootersLocal.push(room+gameSocket.id);
					mySocket = shootersLocal.indexOf(room+gameSocket.id);  
					console.log('private: ' + shootersLocal);
				} 
				else 
				{
					shooters.push(gameSocket.id);
					mySocket = shooters.indexOf(gameSocket.id);  
					console.log(shooters);
				}
				
                dagame = '';
				

                
            if (!priv && shooters.length > 1) {
                console.log('GAME ON.');
                io.sockets.emit('upd',['init']);


                function start_cnt(){
                    timer = new Timer();
                    timer.reset();
                    intID = setInterval(update, 50);
                    io.sockets.emit('upd',['log','its about to go down']);
                    }
                var st_count = (Math.random()*5)+5;
                var st_timer = setInterval(function() {
                    st_count -= Math.floor(Math.random()*2);
                    console.log('timer: '+st_count);
                    if( st_count <= 0) {
                        st_count = 0;
                        clearInterval(st_timer);
                        console.log('timer done.');

                        start_cnt();
                    }
                },200);
            

            }
                
			}else{
				gameSocket.emit('upd', ['full']);
			}

		}else{
			console.log('HaHa. ' + gameSocket.id + ' is a sucka.');
		};
	};

function shot(msg){
        if (dagame == 'over') {
            gameSocket.emit('chat message', 'No shot\'s fired. It\'s over.');
        }
        else{
            if (intID) {clearInterval(intID);}
            gameSocket.broadcast.emit('chat message', gameSocket.id + ' Shot ya after ' + timer.getElapsedTime().toString().substr(0,3) + ' seconds!');
            gameSocket.emit('chat message', 'You shot '+gameSocket.id+' in '+timer.getElapsedTime().toString().substr(0,3) + ' seconds!');
            console.log('some 1 won.');
            gameSocket.emit('upd',['winner']);
            function loser(callback) {
                gameSocket.broadcast.emit('upd',['loser'])
                gameover()
            }
            function gameover() {
                io.sockets.emit('upd',['gameover'])
            };
            loser(gameover);
            shooters = [];
            
        dagame = 'over';
        }
    };

function chatMessage(msg){
		gameSocket.broadcast.emit('chat message', gameSocket.id+': '+msg);
	};

function  disconnectG(){
    	console.log('user disconnected');
		shooters.splice(mySocket, 1);
  };
