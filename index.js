var path = require('path');
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var game = require('./shootout');

app.use(express.static(path.join(__dirname,'public')));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});



io.on('connection', function(socket){
	game.initGame(io, socket);
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});